const mainContainer = document.querySelector('.main-container')
const totalBilling = document.getElementById('total')
const totalTitle = document.getElementById('total-title')

const selectedPlan = document.getElementById('selectedPlan')
const planPrice = document.getElementById('PlanPrice')

const addonOnline = document.getElementById('addon-online')
const onlinePrice = document.getElementById('online-price')

const addonLarger = document.getElementById('addon-larger')
const largerPrice = document.getElementById('larger-price')

const addonCustomizable = document.getElementById('addon-customizable')
const customizablePrice = document.getElementById('customizable-price')

let selectedBillingPlan = false
let total = 0
let planTitle = 'yr'
mainContainer.addEventListener('click', (event) => {
    if (event.target.type === 'submit') {
        event.preventDefault()
        buttonClicked(event)
    }
    else if (event.target.type == 'checkbox') {

        if (event.target.dataset.addon) {

            const data = event.target.dataset.addon.split('/')

            slectedAddOns(data[1], data[0], event.target.checked)

        } else {
            
            changePlan(event.target.checked)
            changeButton()
        }
    } else if (event.target.type == 'change') {
      
        if (planTitle == 'yr') {
            changePlan(false)
        } else {
            changePlan(true)
        }
        changeButton()

    }
    else if (event.target.dataset.selected_plan) {
        
        selectedBillingPlan = true
        const selectedDiv = document.querySelector(`.${event.target.dataset.selected_plan}`)
        const activePlans = document.querySelector('.active-plan')
        if (activePlans) {
            activePlans.classList.remove('active-plan')
        }
        selectedDiv.classList.add('active-plan')
        const price = document.getElementById(`${event.target.dataset.selected_plan}`).textContent

        planSelected(event.target.dataset.selected_plan, price)

    }


})



function buttonClicked(event) {
    const activeSectonNumber = document.querySelector('.active-section')
    
    if (event.target.dataset.button === "form-submit") {
       
        const secondSection = document.getElementById('second')
      
        if (formSubmission()) {
            activeSectonNumber.classList.remove('active-section')
            secondSection.classList.add('active-section')
            changeSection(event.target.id)

        }

    } else if (event.target.dataset.button === 'select-plan' && selectedBillingPlan) {
        activeSectonNumber.classList.remove('active-section')
        const thirdSection = document.getElementById('third')
        thirdSection.classList.add('active-section')

        changeSection(event.target.id)
    }
    else if (event.target.dataset.button === 'add-ons') {
        activeSectonNumber.classList.remove('active-section')
        const fourthSection = document.getElementById('fourth')
        fourthSection.classList.add('active-section')
       
        totalBilling.textContent = `$${total}/${planTitle}`
        changeSection(event.target.id)
    }
    else if (event.target.dataset.button === 'confirm') {
        
        changeSection(event.target.id)
    } else if (event.target.dataset.button == 'back') {
        activeSectonNumber.classList.remove('active-section')
        document.getElementById(event.target.dataset.activesection).classList.add('active-section')
        changeSection(event.target.id)
    }







}

function changeSection(sectionClass) {
    const activeSection = document.querySelector('.active')
    const selectedSection = document.querySelector(`.${sectionClass}`)
    activeSection.classList.remove('active')
    activeSection.classList.add('inactive')

    selectedSection.classList.remove('inactive')
    selectedSection.classList.add('active')
}


function formSubmission() {
    const form = document.querySelector('form')
    const nameInput = document.getElementById('name');
    const emailInput = document.getElementById('email');
    const phoneNumber = document.getElementById('phone-number')

    validateName();

    validateEmail();

    validateNumber()

    if (isFormValid()) {
        return true

    } else {
        return false
    }




    function validateName() {
        const nameValue = nameInput.value.trim();
        const hasNumbersAndSpecialChars = /[0-9!@#$%^&*(),.?":{}|<>]/.test(nameValue)

        if (hasNumbersAndSpecialChars || nameValue === "") {
            displayError(nameInput, 'enter valid first name');

        } else {
            removeError(nameInput);
        }
    }


    function validateEmail() {
        const emailValue = emailInput.value.trim();

        if (emailValue === '' || emailValue.includes('@') === false) {
            displayError(emailInput, 'enter valid email');
        } else {
            removeError(emailInput);
        }
    }

    function validateNumber() {

        const expr = /^(0|91)?[6-9][0-9]{9}$/;
        if (!expr.test(phoneNumber.value)) {
            displayError(phoneNumber, 'enter valid mobile number');
        } else {
            removeError(phoneNumber);
        }
    }

    function displayError(inputElement, errorMessage) {
        const errorElement = inputElement.nextElementSibling;
        errorElement.textContent = errorMessage;
        errorElement.style.display = 'block';
    }


    function removeError(inputElement) {
        const errorElement = inputElement.nextElementSibling;

        errorElement.textContent = '';
        errorElement.style.display = 'none';
    }

    function isFormValid() {
        const errorMessages = form.querySelectorAll('p');
        for (let i = 0; i < errorMessages.length; i++) {
            if (errorMessages[i].textContent !== '') {
                return false;
            }
        }
        return true;
    }

}

function changePlan(flag) {
    
    selectedBillingPlan = false
    const activePlan = document.querySelector('.active-plan')
    if (activePlan) {
        activePlan.classList.remove('active-plan')
    }
    const arcade = document.getElementById('Arcade')
    const advanced = document.getElementById('Advanced')
    const pro = document.getElementById('Pro')

    const onlineService = document.getElementById('online')
    const largerStorage = document.getElementById('larger')
    const customizableProfile = document.getElementById('customizable')
    const twoMonthsFree = document.querySelectorAll('.free')
        ;
    if (flag === true) {
        arcade.textContent = '$90/yr'
        advanced.textContent = '$120/yr'
        pro.textContent = '$150/yr'

        onlineService.textContent = '+$10/yr'
        largerStorage.textContent = '+$20/yr'
        customizableProfile.textContent = '+$20/yr'
        for (let p of twoMonthsFree) {
            p.style.display = 'block'
        }
        totalTitle.textContent = `Total(per year)`
        planTitle = 'yr'

    } else {
        arcade.textContent = '$9/mo'
        advanced.textContent = '$12/mo'
        pro.textContent = '$15/mo'

        onlineService.textContent = '+$1/mo'
        largerStorage.textContent = '+$2/mo'
        customizableProfile.textContent = '+$2/mo'
        for (let p of twoMonthsFree) {
            p.style.display = 'none'
        }
        totalTitle.textContent = `Total(per month)`
        planTitle = 'mo'
    }
}

function planSelected(plan, price) {
    let period = price.split('/')[1]
    const planType = period == 'mo' ? 'monthly' : "yearly"



    selectedPlan.textContent = `${plan}(${planType})`
    
    if(Number(totalBilling.textContent.match(/(\d+)/)[0])>0){
        total=total-Number(planPrice.textContent.match(/(\d+)/)[0])
        total+=Number(price.match(/(\d+)/)[0])
    }else{

        total = Number(price.match(/(\d+)/)[0])
    }
    planPrice.textContent = price



}


function slectedAddOns(addOnName, id, flag) {
    const addonPrice = document.getElementById(`${id}`)


    let title = ''
    let price = ''

    let priceInNumber = Number(addonPrice.textContent.match(/(\d+)/)[0])


    if (flag) {
        title = addOnName
        price = addonPrice.textContent
        total += priceInNumber
    } else {
        total -= priceInNumber
    }

    if (id === 'online') {
        addonOnline.textContent = title
        onlinePrice.textContent = price
    }
    else if (id === 'larger') {
        addonLarger.textContent = title
        largerPrice.textContent = price
    }
    else {
        addonCustomizable.textContent = title
        customizablePrice.textContent = price
    }




}

function changeButton() {
   
   
    total = 0
    let multiplyWith = planTitle == 'yr' ? 10 : 0.1
    let fullTitle = planTitle == 'yr' ? 'yearly' : 'monthly'
    const selectedPlanTitle = selectedPlan.textContent.split('(')
   
    selectedPlan.textContent = `${selectedPlanTitle[0]}(${fullTitle})`

    total += Number(planPrice.textContent.match(/(\d+)/)[0]) * multiplyWith
    planPrice.textContent = `$${Number(planPrice.textContent.match(/(\d+)/)[0]) * multiplyWith}/${planTitle}`


    if (onlinePrice.textContent !== '') {
        total += Number(onlinePrice.textContent.match(/(\d+)/)[0]) * multiplyWith
        onlinePrice.textContent = `+$${Number(onlinePrice.textContent.match(/(\d+)/)[0]) * multiplyWith}/${planTitle}`
    }
    if (largerPrice.textContent !== '') {
        total += Number(largerPrice.textContent.match(/(\d+)/)[0]) * multiplyWith
        largerPrice.textContent = `+$${Number(largerPrice.textContent.match(/(\d+)/)[0]) * multiplyWith}/${planTitle}`

    }

    if (customizablePrice.textContent !== '') {
        total += Number(customizablePrice.textContent.match(/(\d+)/)[0]) * multiplyWith
        customizablePrice.textContent = `+$${Number(customizablePrice.textContent.match(/(\d+)/)[0]) * multiplyWith}/${planTitle}`

    }

    totalBilling.textContent = `$${total}/${planTitle}`

    totalTitle.textContent = `Total(per ${fullTitle.slice(0, -2)})`
}

